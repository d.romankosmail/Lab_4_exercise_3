#include "stdafx.h"
#include <iostream>
#include <cmath>
#include <cstdlib>
#include <ctime>
#define N 2000
#define M 2000
using namespace std;
// ôóíêö³¿
void func11()
{
	double a, b, z;
	cout << "ââåä³òü a" << endl; cin >> a;
	cout << "ââåä³òü b" << endl; cin >> b;
	z = (-4 * (pow((sin((a - b) / 2)), 2)))*(cos(a + b));
	cout.precision(4);
	cout << "z=" << z << endl;
}
// ôàêòîð³àë
int fact(int n)
{
	if (n < 0) // ïåðåâ³ðêà íà â³ä'ºìíå ÷èñëî
		return 0;
	if (n == 0) // ïåðåâ³ðêà íà 0
		return 1;
	else
		return n * fact(n - 1); // ðåêóðñ³ÿ
}
void func112()
{
	double x, s;
	cout << "ââåä³òü x" << endl; cin >> x;
	s = 1 + x + (pow(x, 2) / fact(2)) + (pow(x, 3) / fact(3)) + (pow(x, 4) / fact(4));
	cout << "s=" << s << endl;
}

void func12()
{
	srand(time(0));		// äëÿ òðó ðàíäîìó
	setlocale(0, "ukr"); // óêð ìîâà
	int m, n, k; // îãîëîøåííÿ çì³ííèõ
	cout << "ââåä³òü m" << endl; cin >> m;
	cout << "ââåä³òü n" << endl; cin >> n;
	cout << "ââåä³òü k (3 =< k =< 10)" << endl; cin >> k;	// ââåäåííÿ çíà÷åííü
	if (cin.fail() || k<3 || k>10)  // ïåðåâ³ðêà íà ïðàâèëüíå ââåäåííÿ çíà÷åííü
	{
		cout << "íåêîðåêòíå ââåäåííÿ!" << endl;
		system("pause");
	}
	cout << "Óìîâà À" << endl;
	int i = 0, counter_for_k = 0;	// ³í³ö³àë³çàö³ÿ çì³ííèõ
	for (i = 0; i < m; i++)	// öèêë äëÿ âèâîäó ïåðøî¿ óìîâè
	{
		counter_for_k++;
		cout << 77 + rand() % 51 << "; ";	// âèâ³ä óìîâè À
		if (counter_for_k == k)
		{
			cout << endl;
			counter_for_k = 0;
		}
	}
	cout << "ê³íåöü óìîâè À" << endl << "ïî÷àòîê óìîâè B" << endl;
	i = 0, counter_for_k = 0;	// îáíóëåííÿ ë³÷èëüíèê³â
	for (i = 0; i < n; i++)	// öèêë äëÿ âèâîäó óìîâè B
	{
		counter_for_k++;
		cout << (-6 + rand() % 17) + (0 + rand() % 10)*0.1 << "; ";	// âèâ³ä óìîâè B
		if (counter_for_k == k)
		{
			cout << endl;
			counter_for_k = 0;
		}
	}
}

void func13() 
{
	srand((unsigned int)time(NULL));
	setlocale(0, "ukr");
	int *n = new int(0);
	int *m = new int(0);
	int a, b;
	cout << "ââåä³òü N" << endl; cin >> *n;
	cout << "ââåä³òü M" << endl; cin >> *m;
	cout << "ââåä³òü a (a>b)" << endl; cin >> a;
	cout << "ââåä³òü b (a>b)" << endl; cin >> b;

	if (cin.fail() || *n>N || *m>M || a>b)
	{
		cout << "íåïðàâèëüíå ââåäåííÿ" << endl;
		system("pause");
	}

	int i = 0, j = 0, max_element_value = 0;
	int** arr; //a - âêàç³âíèê íà ìàñèâ âêàç³âíèê³â
	arr = new int*[*n]; //âèä³ëåííÿ ïàì’ÿò³ äëÿ ìàñèâó âêàç³âíèê³â íà n ðÿäê³â
	for (int i = 0; i < *m; i++)
	{
		arr[i] = new int[*m]; //âèä³ëåííÿ ïàì’ÿò³ äëÿ êîæíîãî ðÿäêà ìàñèâó ðîçì³ðí³ñòþ m
	}
	for (i = 0; i < *n; i++)
	{
		for (j = 0; j < *m; j++)
		{
			arr[i][j] = a + rand() % (b - a + 1);
			cout << "[" << i << "][" << j << "]= " << arr[i][j] << endl;

			if (i == 0) { max_element_value = arr[0][0]; }
			if (abs(max_element_value) < abs(arr[i][j]))
			{
				max_element_value = arr[i][j];
			}
		}
	}
	cout << "ìàêñèìàëüíèé çà ìîäóëåì åëåìåíò" << max_element_value << endl;
}
// ãîëîâíà ôóíêö³ÿ
int main()
{
	setlocale(0, "ukr");
	char i;
	do {
		cout << "1 - 1.1; 2 - 1.1.2; 3 - 2.1; 4 - 3.1, q- âèõ³ä " << endl;
		cin >> i;
		switch (i)
		{
			case '1': func11(); break;
			case '2': func112(); break;
			case '3': func12(); break;
			case '4': func13(); break;
			case 'q': break;
			default:
			{
				cout << "íåïðàâèëüíå ââåäåííÿ" << endl;
				break;
			}
		}
	} while (i != 'q');
	system("pause");
    return 0;
}